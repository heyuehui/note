class GroupsController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update]
  before_action :correct_user,   only: [:edit, :update]
  before_action :admin_user,     only: [:destroy]

  def index
    @groups = Group.paginate(page: params[:page])
  end
  
  def new
    @group = Group.new
  end
  
  def create
    @group = Group.new(group_params)
    
    if current_user.groups.create(group_params)
      flash[:success] = "Success Create Group"
      redirect_to joined_groups_user_path(current_user.id)
    else
      render 'new'
    end
  end

  def show
    @group = Group.find(params[:id])
  end

  def edit
    @group = Group.find(params[:id])
  end
  
  def update
    @group = Group.find(params[:id])
    if @group.update_attributes(group_params)
      flash[:success] = "Group updated"
      redirect_to @group
    else
      render 'edit'
    end
  end
  
  def destroy
    
    @group = Group.find(params[:id])
    if(@group.users.empty?)
      if @group.destroy
        flash[:success] = "deleted group"
      end
    else
      flash[:failure] = "deleted group"
      redirect_to joined_groups_user_path
    end
  end

  
  def members(group)
    @title = "Members"
    @members = group.users
    render 'members'
  end
  
  private

  def group_params
    params.require(:group).permit(:name)
  end

  def correct_user
    redirect_to(root_url) unless current_user.groups.find(params[:id])
  end

  def admin_user
    redirect_to(root_url) unless current_user.admin?
  end
  
end
