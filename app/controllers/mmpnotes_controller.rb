class MmpnotesController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy, :show, :edit, :update]
  before_action :correct_user, only: [:destroy, :edit, :update] 

  def new
    @mmpnote = Mmpnote.new
    @group = Group.find(params[:group_id])
  end
  
  def create
    if(params[:group_id])
      @group = Group.find(params[:group_id])
      @mmpnote = @group.mmpnotes.build(mmpnote_params)
      if @mmpnote.save
        flash[:success] = "Group Mmpnote Created!"
        redirect_to @group
      else
        flash[:failure] = "Fail to save"
        redirect_to 'static_pages/home'
      end
    else
    @mmpnote = current_user.mmpnotes.build(mmpnote_params)
    if @mmpnote.save
      flash[:success] = "Mmpnote created!"
      redirect_to root_url
    else
      render 'static_pages/home'
    end
    end
  end

  def edit
    @mmpnote = Mmpnote.find(params[:id])
  end

  def update
    @mmpnote = Mmpnote.find(params[:id])
    if @mmpnote.update_attributes(mmpnote_params)
      flash[:success] = "Note updated"
      redirect_to @mmpnote
    else
      render 'edit'
    end
  end
  
  def show
    @mmpnote = Mmpnote.find(params[:id])
  end

  def destroy
    Mmpnote.find(params[:id]).destroy
    flash[:success] = "note deleted"
    redirect_to current_user
  end

  private

  def mmpnote_params
    params.require(:mmpnote).permit(:content, :subject)
  end
  
  def correct_user
    @mmpnote = current_user.mmpnotes.find_by(id: params[:id])
    redirect_to root_url if @mmpnote.nil?
  end
end
