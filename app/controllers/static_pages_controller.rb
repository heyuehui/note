class StaticPagesController < ApplicationController
  def home
    @mmpnote = current_user.mmpnotes.build if logged_in?
  end

  def help
  end

  def about
  end
end
