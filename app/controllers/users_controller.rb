# coding: utf-8
class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :correct_user,   only: [:eidt, :update]
  before_action :admin_user,     only: [:destroy]
  
  def index
    @users = User.paginate(page: params[:page])
  end
  
  def new
    @user = User.new
  end
  
  def show
    @user = User.find(params[:id])
    @mmpnotes = @user.mmpnotes.paginate(page: params[:page])
  end
  
  def create
    @user = User.new(user_params)
                                       
    if @user.save
      log_in @user
      flash[:success] = "Welcome to MMP NOTE!"
      redirect_to @user
    else
      render 'new'
    end
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success]  = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end
  
  def joined_groups
    @user = User.find(params[:id])
    @title = "Groups"
    @joined_groups = current_user.groups.all
    render 'joined_groups'
  end

  def apply_for_join
    @group = Group.find(params[:id])
    if(current_user.join_group(@group))
      flash[:success] = "success join group"
      redirect_to groups_path
    end
  end
  private
  
  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end

  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless @user == current_user
  end

  def admin_user
    redirect_to(root_url) unless current_user.admin?
  end
  
end
