class Group < ApplicationRecord
  has_many :relationships, foreign_key: "group_id", dependent: :destroy
  has_many :users, through: :relationships
  has_many :mmpnotes

  def add_member(other_user)
    users << other_user 
  end
  
  def delete_member(other_user)
    users.delete(other_user)
  end
end
