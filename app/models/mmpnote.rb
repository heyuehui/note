class Mmpnote < ApplicationRecord
  default_scope -> { order(created_at: :desc) }
  validates :content, presence: true, length: { maximum: 4000}
  validates :subject, length: { maximum: 50 }
end
