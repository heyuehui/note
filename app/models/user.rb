#1 coding: utf-8
class User < ApplicationRecord
  has_many :mmpnotes, dependent: :destroy
  has_many :relationships, foreign_key: "user_id", dependent: :destroy
  has_many :groups, through: :relationships
  before_save{ self.email = email.downcase }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :name, presence: true, length: {maximum: 50}
  validates :email, presence: true, length: {maximum: 255},
            format: { with: VALID_EMAIL_REGEX },
            uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true

  #返回指定字符串的哈希摘要
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
             BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  def join_group(other_group)
    groups << other_group
  end

  def exit_group(other_group)
    groups.delete(other_group)
  end
end
