Rails.application.routes.draw do
  get 'relationships/destroy'

  get 'sessions/new'

  root 'static_pages#home'
  get '/help', to: 'static_pages#help'
  get '/about', to: 'static_pages#about'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get '/signup', to: 'users#new'
  post '/signup', to: 'users#create'
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  resources :users do
    member do
      get :joined_groups
    end
  end
  resources :users do
    member do
      post :apply_for_join
    end
  end
  resources :mmpnotes do
    member do
      post :create_for_group
    end
  end
  resources :users
  resources :mmpnotes, only: [:create, :destroy, :show, :edit, :update]
  resources :groups
  resources :relationships, only: [:create, :destroy]
end
