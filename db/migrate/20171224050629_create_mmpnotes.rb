class CreateMmpnotes < ActiveRecord::Migration[5.1]
  def change
    create_table :mmpnotes do |t|
      t.text :content
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :mmpnotes, [:user_id, :created_at]
  end
end
