class AddSubjectToMmpnotes < ActiveRecord::Migration[5.1]
  def change
    add_column :mmpnotes, :subject, :string
  end
end
