class AddGroupIdToMmpnote < ActiveRecord::Migration[5.1]
  def change
    add_column :mmpnotes, :group_id, :integer
    add_index :mmpnotes, :group_id
  end
end
