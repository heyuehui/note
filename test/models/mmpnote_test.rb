require 'test_helper'

class MmpnoteTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = users(:dagen)
    @mmpnote = @user.mmpnotes.build(content: "Hello World")
  end

  test "should be valid" do
    assert @mmpnote.valid?
  end

  test "user id should be present" do
    @mmpnote.user_id = nil
    assert_not @mmpnote.valid?
  end
  
  test "content should be present" do
    @mmpnote.content = "   "
    assert_not @mmpnote.valid?
  end

  test "content should be at most 4000 characters" do
    @mmpnote.content = "a" * 4001
    assert_not @mmpnote.valid?
  end

  test "order should be most recent first" do
    assert_equal mmpnotes(:most_recent), Mmpnote.first
  end
  
end
